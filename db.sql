create table usuarios(
	id int not null PRIMARY KEY AUTO_INCREMENT,
	usuario varchar(100) not null,
	email varchar(100) not null,
	senha varchar (32)

);

create table comentarios(
id_comentarios int null PRIMARY KEY AUTO_INCREMENT,
id_usuario int null,
comentarios varchar(200) not null,
data_inclusao datetime default CURRENT_TIMESTAMP
);

create table usuarios_seguidores(
id_usuario_seguidor int not null PRIMARY KEY AUTO_INCREMENT,
id_usuario int not null,
seguindo_id_usuario int not null,
data_registro datetime default CURRENT_TIMESTAMP
);

$sql = " SELECT u.*, us.* ";
$sql.= " FROM usuarios AS u ";
$sql.= " LEFT JOIN usuarios_seguidores AS us ";
$sql.= " ON (us.id_usuario = 3 ' AND u.id = us.seguindo_id_usuario) "; 
$sql.= " WHERE u.usuario like '%a%' AND u.id <> 3 ";